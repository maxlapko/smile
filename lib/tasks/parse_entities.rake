namespace :parsers do
  desc "Parse statuses from statuses.su"
  task :parse_statuses => :environment do
    require_relative "../StatusParser"
    StatusParser.new.perform
  end

  desc "Parse anecdots from bestjoke.org.ua"
  task :parse_anecdots => :environment do
    require_relative "../AnecdotParser"
    AnecdotParser.new.perform
  end

  task :all => [:parse_statuses, :parse_anecdots]
end
