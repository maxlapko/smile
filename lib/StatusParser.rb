# -*- coding: utf-8 -*-

#Parser statuses.su
require 'open-uri'
require 'nokogiri'

class StatusParser
  MAIN_URL = 'http://statuses.su'
  def initialize
    @status_count = 0
    @repeat_count = 0
    @first_run = true
  end

  def perform
    status_code = EntityType.find_by_code 'Status'
    begin
      get_categories do |cat|
        @repeat_count = 0
        unless cat[:small].nil?
          cat[:title] = 'Без категории'
        end
        category = Category.find_or_create_by_name_and_entity_type_id(cat[:title], status_code.id)
        get_statuses_from_page(cat[:href], category.id)
      end
    rescue Exception => e
      puts e.message
    end
    puts "Count parsed statuses: #{@status_count}"
  end

  def get_categories
    load_html MAIN_URL
    @html.css('.categoryes_b a').each do |category|
      yield Hash[:href, "#{MAIN_URL}#{category['href']}", :title, category.content.strip]
    end
    @html.css('.categoryes a').each do |category|
      yield Hash[:href, "#{MAIN_URL}#{category['href']}", :title, category.content.strip, :small, true]
    end
  end

  def get_statuses_from_page(href, category_id)
    puts href
    load_html href
    @html.css('#dle-content div.cat_cont_text').each_with_index do |status, number|
      if !@first_run and @repeat_count > 10
        puts "Found same statuses in category #{category_id}"
        return true
      end
      text = prepare_text(status.text)
      unless Status.find_by_content(text)
        @status_count += 1
        Status.new do |s|
          s.category_id = category_id
          s.content     = text
          s.user_id     = 2
          s.status      = Status::APPROVED_STATUS
          s.save! :validate => false
        end
      else
        @repeat_count += 1
      end
    end
    if next_page = @html.css('#but_next a') and next_page.count > 0
      get_statuses_from_page(next_page.first['href'].strip, category_id)
    end
  end

  protected
  def prepare_text(text)
    text.gsub(/<br\s*\/?>/, "\n").strip
  end

  def load_html(html)
    @html = Nokogiri::HTML open(html)
  end
end