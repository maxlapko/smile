# -*- coding: utf-8 -*-

#Parser http://bestjoke.org.ua
require 'open-uri'
require 'nokogiri'

class AnecdotParser
  MAIN_URL = 'http://bestjoke.org.ua'

  def initialize
    @anecdot_count = 0
    @repeat_count = 0
    @first_run = true
  end

  def perform
    anecdot_code = EntityType.find_by_code 'Anecdot'
    begin
      get_categories do |cat|
        @repeat_count = 0
        category = Category.find_or_create_by_name_and_entity_type_id(cat[:title], anecdot_code.id)
        get_anecdotes_from_page(cat[:href], category.id)
      end
    rescue Exception => e
      puts e.message
    end
    puts "Count parsed anecdotes: #{@anecdot_count}"
  end

  def get_categories
    load_html MAIN_URL

    @html.css('.leftBG li a').each do |category|
      yield Hash[:href, "#{MAIN_URL}/#{category['href']}", :title, category.content.strip]
    end
  end

  def get_anecdotes_from_page(href, category_id)
    if active_page = /q=(\d+)/.match(href)
      active_page = active_page[1].to_i  
    else
      href += '?q=1' 
      active_page = 1
    end  
    puts href
    load_html href      
    
    @html.css('div.joke').each_with_index do |anecdot, number|
      if !@first_run and @repeat_count > 10
        puts "Found same anecdots in category #{category_id}"
        return
      end
      text = prepare_text(anecdot.text)
      unless Anecdot.find_by_content(text)
        @anecdot_count += 1
        Anecdot.new do |a|
          a.category_id = category_id
          a.content     = text
          a.user_id     = 2
          a.status      = Anecdot::APPROVED_STATUS
          a.save! :validate => false
        end
      else
        @repeat_count += 1 
      end
    end
    
    if next_page = @html.css('span.list a[href="' +  href.gsub(/q=(\d+)/, "q=#{active_page + 1}").sub(MAIN_URL, '')   + '"]') and next_page.count > 0
      get_anecdotes_from_page(MAIN_URL + next_page.first['href'].strip, category_id)
    end
  end

  protected
  def prepare_text(text)
    text.gsub(/<br\s*\/>/, "\n").strip
  end

  def load_html(html)
    @html = Nokogiri::HTML open(html)
  end
end