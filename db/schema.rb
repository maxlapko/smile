# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120215121332) do

  create_table "active_admin_comments", :force => true do |t|
    t.integer  "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "entity_type_id"
  end

  add_index "categories", ["entity_type_id"], :name => "index_categories_on_entity_type_id"
  add_index "categories", ["name"], :name => "index_categories_on_name", :length => {"name"=>191}

  create_table "comments", :force => true do |t|
    t.text     "content"
    t.integer  "user_id"
    t.integer  "entity_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["entity_id"], :name => "entity_id_idx"
  add_index "comments", ["user_id"], :name => "user_id_idx"

  create_table "entities", :force => true do |t|
    t.integer  "user_id",                                   :null => false
    t.text     "content"
    t.integer  "category_id"
    t.string   "status",        :default => "not_approved"
    t.string   "type",                                      :null => false
    t.integer  "rating",        :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "comment_count", :default => 0
  end

  add_index "entities", ["category_id"], :name => "index_entities_on_category_id"
  add_index "entities", ["status"], :name => "index_entities_on_status", :length => {"status"=>191}
  add_index "entities", ["type"], :name => "index_entities_on_type", :length => {"type"=>191}
  add_index "entities", ["user_id"], :name => "index_entities_on_user_id"

  create_table "entity_types", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "entity_types", ["code"], :name => "code_idx", :length => {"code"=>191}

  create_table "favorites", :force => true do |t|
    t.integer  "user_id"
    t.integer  "entity_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "favorites", ["entity_id"], :name => "index_favorites_on_entity_id"
  add_index "favorites", ["user_id"], :name => "index_favorites_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "firstname",                         :null => false
    t.string   "lastname",                          :null => false
    t.string   "auth_key",                          :null => false
    t.string   "auth_url"
    t.string   "auth_type",                         :null => false
    t.string   "sex"
    t.string   "description"
    t.integer  "rating",        :default => 0
    t.string   "role",          :default => "user"
    t.string   "avatar_url"
    t.string   "big_photo_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["auth_key"], :name => "index_users_on_auth_key", :length => {"auth_key"=>191}
  add_index "users", ["auth_type"], :name => "index_users_on_auth_type", :length => {"auth_type"=>191}

  create_table "votes", :force => true do |t|
    t.integer  "user_id"
    t.integer  "entity_id"
    t.integer  "rating"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["entity_id"], :name => "entity_id_idx"
  add_index "votes", ["user_id"], :name => "user_id_idx"

end
