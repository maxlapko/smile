class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.integer :user_id
      t.integer :entity_id
      t.integer :rating

      t.timestamps
    end
    add_index :votes, :user_id
    add_index :votes, :entity_id
  end
end
