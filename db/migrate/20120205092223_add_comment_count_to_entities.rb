class AddCommentCountToEntities < ActiveRecord::Migration
  def change
    add_column :entities, :comment_count, :integer, :default => 0
  end
end
