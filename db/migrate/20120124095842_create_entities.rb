class CreateEntities < ActiveRecord::Migration
  def change
    create_table :entities do |t|
      t.integer :user_id,     :null => false
      t.text :content
      t.integer :category_id
      t.string :status,       :default => 'not_approved'
      t.string :type,         :null => false
      t.integer :rating,      :default => 0
      t.timestamps
    end
    add_index :entities, :user_id
    add_index :entities, :category_id
    add_index :entities, :status
    add_index :entities, :type

  end
end
