class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :content, :null => false
      t.references :user
      t.references :entity

      t.timestamps
    end

    add_index :comments, :user_id
    add_index :comments, :entity_id
  end
end
