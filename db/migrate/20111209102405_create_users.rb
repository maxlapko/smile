class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string :firstname,        :null => false
      t.string :lastname,         :null => false
      t.string :auth_key,         :null => false
      t.string :auth_url
      t.string :auth_type,        :null => false
      t.string :sex
      t.string :description
      t.integer :rating,          :default => 0
      t.string :role,             :default => 'user'
      t.string :avatar_url
      t.string :big_photo_url

      t.timestamps
    end
    add_index :users, :auth_key
    add_index :users, :auth_type
  end

  def self.down
    drop_table :users
  end
end