class AddEntityTypeIdToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :entity_type_id, :integer
    add_index :categories, :entity_type_id
  end
end
