class CreateEntityTypes < ActiveRecord::Migration
  def change
    create_table :entity_types do |t|
      t.string :name
      t.string :code
      t.timestamps
    end
    add_index :entity_types, :code
  end
end
