class User < ActiveRecord::Base
  ADMIN_ROLE  = 'admin'
  USER_ROLE   = 'user'
  SEX = {
      :male   => 'male',
      :female => 'female',
      :none   => 'none'
  }

  attr_accessor :voted_store, :favorite_store

  has_many :entities
#  has_many :statuses
#  has_many :anecdots
  has_many :votes
  has_many :voted_entities, :class_name => 'Entity', :through => :votes, :source => :entity
  has_many :comments
  has_many :favorites, :dependent => :destroy
  has_many :favorite_entities, :class_name => 'Entity', :through => :favorites, :source => :entity

  def self.create_from_vk(auth_params)
    create! do |user|
      user_data = auth_params['extra']['raw_info']
      user.auth_key = auth_params['uid']
      user.firstname = user_data['first_name']
      user.lastname = user_data['last_name']
      user.auth_type = auth_params['provider']
      user.auth_url = 'http://vkontakte.ru/id' + auth_params['uid'].to_s
      user.role = USER_ROLE
      user.sex = user_data['sex'] == 2 ? SEX[:male] : (user_data['sex'] == 1 ? SEX[:female] : SEX[:none])
      user.description = ''
      user.rating = 0
      user.avatar_url = user_data['photo']
      user.big_photo_url = user_data['photo_big']
    end
  end

  def is_admin?
    self.role == ADMIN_ROLE
  end

  def full_name
    self.firstname + ' ' + self.lastname
  end

end
