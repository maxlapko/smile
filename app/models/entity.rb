# -*- coding: utf-8 -*-
class Entity < ActiveRecord::Base
  APPROVED_STATUS = 'approved'
  DELETED_STATUS = 'deleted'
  NOT_APPROVED_STATUS = 'not_approved'

  before_validation :clear_content_and_set_status

  belongs_to :user
  belongs_to :category
  has_many :comments, :dependent => :delete_all
  has_many :votes, :dependent => :delete_all

  validates :content,     :presence  => true
  validate :check_type

  scope :approved, where(:status => APPROVED_STATUS)
  scope :recent,   order('updated_at DESC')
  scope :popular,  order('rating DESC')

  def not_approved?
    self.status == NOT_APPROVED_STATUS
  end

  def check_type
    errors.add(:type, 'имеет не корректное значение.') unless EntityType.all.map(&:code).include?(self.type)
  end

protected
  def clear_content_and_set_status
    self.content = self.content.to_s.gsub(/\s+/, ' ').strip
    self.status = self.user.is_admin? ? APPROVED_STATUS : NOT_APPROVED_STATUS
  end

end
