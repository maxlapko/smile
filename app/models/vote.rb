class Vote < ActiveRecord::Base
  after_create :increment_entity_rating
  belongs_to :user
  belongs_to :entity

  def increment_entity_rating
  	Entity.update_counters(self.entity_id, :rating => self.rating)
  end  
end
