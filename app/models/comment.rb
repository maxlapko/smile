class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :entity

  after_destroy :decrease_comment_count
  after_create  :increase_comment_count

  validates_presence_of :content

  before_validation :trim_content

  def trim_content
    self.content = self.content.gsub(/\s+/, ' ').strip
  end

  protected

  def decrease_comment_count
    Entity.update_counters(self.entity_id, :comment_count => -1)
  end

  def increase_comment_count
    Entity.update_counters(self.entity_id, :comment_count => 1)
  end
end
