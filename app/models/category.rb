class Category < ActiveRecord::Base
  has_many :anecdots
  has_many :statuses
end
