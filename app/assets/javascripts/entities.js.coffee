# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
class Entity
  constructor: ->
    $('a.approve_link, a.remove_link, a.favorite_link').live 'ajax:beforeSend', (evt, xhr, settings) ->
      $(this).addLoader()

    $('a.approve_link').live 'ajax:complete', (evt, xhr, status) ->
      if status == 'success'
        $.jGrowl('Запись успешно утверждена.');
        rsp = $.parseJSON xhr.responseText
        $(this).hideLoader(!rsp.success)

    $('a.remove_link').live 'ajax:complete', (evt, xhr, status) ->
      if status == 'success'
        rsp = $.parseJSON xhr.responseText
        $(this).hideLoader(!rsp.success)
        if rsp.success
          $.jGrowl('Запись успешно удалена.');
          if $(this).parents('div.comment').length > 0
            $(this).parents('div.comment').fadeOut(200)
          else if $(this).parents('div.entity').length > 0
            $(this).parents('div.entity').fadeOut(200)

    if $('.pagination').length
      $(window).scroll ->
        url = $('.pagination a.next_page').attr('href')
        if url && $(window).scrollTop() > $(document).height() - $(window).height() - 150
          $('.pagination').text('Загружаю ...')
          $.getScript(url)
      $(window).scroll()

jQuery ->
  new Entity()