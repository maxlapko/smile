// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require js/bootstrap-dropdown
//= require js/bootstrap-modal
//= require js/bootstrap-tooltip
//= require jquery.jgrowl
//= require jquery-expander
//= require_tree .

/*VK functions*/
var VKnet = {
  w: 820,
  resize: function(height) {
    if (!height) {
      height = $('html').height();
		  height = (height < 100 ? 400 : height);
    }
    VK.callMethod('resizeWindow', this.w, height + 75);
  },
  getInviteBox: function () {
    VK.callMethod('showInviteBox');
  },
  postToWall: function(user_id, message, attachment) {
    var params = {
      message: message
    };
    if (user_id) {
      params.owner_id = user_id;
    }
    if (attachment) {
      params.attachment = attachment;
    }
    VK.api('wall.post', params, function(rsp) {});
  }
}

$.fn.flashMessage = function() {
  var $self = $(this);
  $self.fadeIn(500, function() {
    setTimeout(function(){
      $self.fadeOut(500);
    }, 2000);
  });
}

$.fn.addLoader = function() {
  $(this).hide();
  $('<span class="loader">Обрабатываю ...</span>').insertAfter($(this))
}

$.fn.hideLoader = function(show_element) {
  $(this).next('span.loader').remove();
  if (show_element) {
    $(this).show();
  }
}

$.fn.customFormSubmit = function() {
  $(this)
    .bind("ajax:beforeSend", function(evt, xhr, settings){
      $(this).find('input[name="commit"]').addLoader();
    })
    .bind('ajax:complete', function(evt, xhr, status){
      $(this).find('input[name="commit"]').hideLoader(true);
    });
}

$(function() {
  /* jGrowl settings */
  $.jGrowl.defaults.life = 2000;
  $.jGrowl.defaults.closerTemplate = '<div>[ Закрыть все ]</div>';

  $('#right_navigation').dropdown();
  if ($('div.alert-message').length > 0) {
    $('div.alert-message').flashMessage();
  }

  $('a.entity_text').expander({
    expandText: 'читать полностью',
    userCollapse: false,
    slicePoint: 300
  });

  if ($('#modal-form').length) {
    $('#modal-form').modal({
      backdrop: true,
      keyboard: true
    });
  }

  if ($('#new_comment').length > 0) {
    $('#new_comment').customFormSubmit();
  }

  var $win = $(window),
    $nav = $('.category'),
    navTop = $('.category').length && $('.category').offset().top - 40,
    isFixed = 0;
    processScroll();
  $win.on('scroll', processScroll);
  function processScroll() {
    var scrollTop = $win.scrollTop();
    if (scrollTop >= navTop && !isFixed) {
      isFixed = 1;
      $nav.addClass('category-fixed');
    } else if (scrollTop <= navTop && isFixed) {
      isFixed = 0;
      $nav.removeClass('category-fixed');
    }
  }
});