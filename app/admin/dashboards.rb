# -*- coding: utf-8 -*-
ActiveAdmin::Dashboards.build do
  section "Недавние неутверждённые сущности" do
    table_for Entity.recent.limit(5) do
      column :content
      column :status
      column :created_at
    end
    strong { link_to "Просмотреть всё", admin_entities_path }
  end

  section "Недавние юзеры" do
    table_for User.order('created_at DESC').limit(5) do
      column :full_name do |user|
        link_to user.full_name, user.auth_url
      end
      column :auth_url
    end
    strong { link_to "Просмотреть всё", admin_entities_path }
  end
end