module ApplicationHelper
  def title(page_title)
    content_for(:title) { page_title.to_s }
  end

  def page_title (page_title)
    content_for(:page_title) { page_title.to_s }
  end

  def stylesheet(*args)
    content_for(:head) { stylesheet_link_tag(*args) }
  end

  def javascript(*args)
    content_for(:head) { javascript_include_tag(*args) }
  end

  def controller?(*controller)
    controller.include?(params[:controller])
  end

  def action?(*action)
    action.include?(params[:action])
  end

  def nav_link(link_text, link_path, active = false)
    class_name = active ? 'active' : ''
    content_tag(:li, :class => class_name) do
      link_to link_text, link_path
    end
  end

  def nl2br(string)
    string.gsub /\n+\s*/, '<br>'
  end

end
