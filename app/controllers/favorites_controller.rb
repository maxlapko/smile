class FavoritesController < ApplicationController
  before_filter :require_login

  def index
    @entities = current_user.favorite_entities.includes(:user).order('favorites.created_at DESC').page(params[:page]).per_page(20)
  end

  def create
    @entity = Entity.find params[:entity_id]
    render :json => { :success => false } if current_user.favorite_entities.exists? @entity
    current_user.favorites.create!(:entity => @entity)
  end

  def destroy
    @entity = Entity.find params[:id]
    render :json => { :success => false } unless current_user.favorite_entities.exists? @entity
    current_user.favorite_entities.delete(@entity)
    render :json => { :success => true }
  end

end
