class VotesController < ApplicationController
  before_filter :require_login

  def create
    @entity = Entity.find params[:entity_id]
    render :json => { :success => false } if current_user.voted_entities.exists? @entity
    rating = params[:rate] == 'plus' ? 1 : -1
    current_user.votes.create!(:entity => @entity, :rating => rating)
    current_user.voted_store = [@entity]
    @entity.rating += rating
  end

end
