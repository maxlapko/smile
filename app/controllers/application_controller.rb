# -*- coding: utf-8 -*-
class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user, :is_logged?, :is_admin?

  def not_authenticated
    respond_to do |format|
      format.html { redirect_to root_url, :alert => 'У вас недостаточно прав для данной операции.' }
      format.js { render :json => {:success => false } }
    end
  end

  def require_login
    not_authenticated unless is_logged?
  end

  protected

  def require_admin
    not_authenticated unless is_admin?
  end

  def is_admin?
    is_logged? && current_user.is_admin?
  end

  private

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def is_logged?
    session[:user_id]
  end
end
