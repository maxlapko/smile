# -*- coding: utf-8 -*-
class EntitiesController < ApplicationController
  before_filter :require_login, :only => [:create]
  before_filter :set_entity, :require_admin, :only => [:approve, :destroy]

  def index
    @entities = entities_type.includes(:user).page(params[:page]).per_page(20)
    unless is_admin?
      @entities = @entities.approved
    end
    if params[:category_id] and @category = Category.find(params[:category_id])
      @entities = @entities.where(:category_id => @category.id)
    end
    @entities = entities_order
    if is_logged?
      current_user.voted_store = current_user.voted_entities.where(:id => @entities.map(&:id))
      current_user.favorite_store = current_user.favorite_entities.where(:id => @entities.map(&:id))
    end
  end

  def show
    @entity = Entity.includes(:user, :comments => :user).find params[:id]
    if is_logged?
      current_user.voted_store = current_user.voted_entities.where(:id => @entity.id)
      current_user.favorite_store = current_user.favorite_entities.where(:id => @entity.id)
    end
  #rescue ActiveRecord::RecordNotFound
  #  redirect_to root_path, :alert => 'Запрашиваемая страница не найдена.'
  end

  def new
    @entity = Entity.new
  end

  def create
    @entity = current_user.entities.build params[:entity]
    @entity.type = params[:entity][:type]
    @entity.save
  end

  def approve
    render :json => { :success => @entity.update_attribute(:status, Entity::APPROVED_STATUS) }
  end

  def destroy
    @entity.destroy
    render :json => { :success => true }
  end

  private

  def entities_order
    case params[:order]
      when 'popular' then @entities.popular
      else @entities.recent
    end
  end

  def entities_type
    case params[:type]
      when 'anecdot' then Anecdot
      when 'status'  then Status
      else Entity
    end
  end

  def set_entity    
    @entity = Entity.find params[:id]
  end

end
