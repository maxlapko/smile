# -*- coding: utf-8 -*-
class SessionsController < ApplicationController
  def create
    auth = request.env["omniauth.auth"]
    user = User.find_by_auth_key_and_auth_type(auth['uid'], auth['provider'])
    unless user
      user = User.create_from_vk(auth) if auth['provider'] == 'vkontakte'
    end
    session[:user_id] = user.id
    redirect_to root_url, :notice => 'Успешно авторизированы.'
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end

  #dummy action
  def login
    user = User.find_by_auth_key params[:auth_key]
    session[:user_id] = user.id
    redirect_to root_url, :notice => 'Успешно авторизированы.'
  end
end
