class CommentsController < ApplicationController
  before_filter :require_login, :only => [:create]
  before_filter :require_admin, :only => [:destroy]

  def new
    @entity = Entity.find(params[:entity_id])
    @comment = @entity.comments.build
  end

  def create
    @entity = Entity.find(params[:entity_id])
    @comment = @entity.comments.build(params[:comment])
    @comment.user = current_user
    @comment.save
  end

  def destroy
    @comment = Comment.find params[:id]
    @comment.destroy
    render :json => { :success => true }
  end

end
